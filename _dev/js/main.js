(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.MobileButton = {
        toggleState: function(event) {
            event.preventDefault();

            var $handle = $(this),
                $nav    = $('#nav-mobile');

            $nav.slideToggle();
            $handle.toggleClass('close');
        },

        init: function() {
            $('#mobile-toggle').on('click touchstart', this.toggleState);
        }
    };

    App.Home = {
        bannersCycle: function() {
            $('#banners-cycle').cycle({
                slides: '>a'
            });
        },

        init: function() {
            if ($('.main').hasClass('home')) {
                this.bannersCycle();
            }
        }
    };

    App.Noticias = {
        chamadasCycle: function() {
            $('#noticias-cycle').cycle({
                slides: '>a',
                pager: '.noticias-cycle-pager',
                pagerTemplate: '<a href="{{href}}">{{title}}<span>LER MAIS »</span></a>'
            });

            $('.noticias-cycle-pager.pager-aside a').on('click touchstart', function() {
                window.location.href = this.href;
            });
        },

        listaNoticiasAnos: function(event) {
            event.preventDefault();

            var $wrapper = $(this).parent(),
                $lista   = $(this).next(),
                $other   = $('.lista-wrapper').not($wrapper);

            if ($wrapper.hasClass('open')) return;

            $other.find('div').slideUp('slow', function () {
                $other.removeClass('open');
            });
            $lista.slideDown('slow', function () {
                $wrapper.addClass('open');
            });
        },

        init: function() {
            if ($('.main').hasClass('noticias')) {
                this.chamadasCycle();
                $('.lista-wrapper .trigger').on('click touchstart', this.listaNoticiasAnos);
            }
        }
    };

    App.Contato = {
        toggleForms: function(event) {
            event.preventDefault();

            var $wrapper = $(this).parent(),
                $form    = $(this).next(),
                $other   = $('.form-wrapper').not($wrapper);

            if ($wrapper.hasClass('open')) return;

            $other.find('form').slideUp('slow', function () {
                $other.removeClass('open');
            });
            $form.slideDown('slow', function () {
                $wrapper.addClass('open');
            });
        },

        curriculoFileInput: function(event) {
            if (event.target.files.length < 1) {
                $('#curriculo div').text('ANEXAR CURRÍCULO').parent().removeClass('active');
            } else {
                $('#curriculo div').text(event.target.files[0].name).parent().addClass('active');
            }
        },

        init: function() {
            if ($('.main').hasClass('contato')) {
                $('.form-wrapper a').on('click touchstart', this.toggleForms);
            }

            if ($('#curriculo').length) {
                $('#curriculo input').on('change', this.curriculoFileInput);
            }
        }
    };

    App.CertificacaoSistemas = {
        accordionToggle: function(event) {
            event.preventDefault();

            var $handle  = $(this),
                $content = $handle.next(),
                $other   = $('.accordion a').not($handle);

            if ($handle.hasClass('open')) {
                $content.slideUp('slow', function() {
                    $handle.removeClass('open');
                });
                return;
            }

            $other.next().slideUp('slow', function () {
                $other.removeClass('open');
            });
            $content.slideDown('slow', function () {
                $handle.addClass('open');
            });
        },

        init: function () {
            if ($('.main').hasClass('certificacao-sistemas')) {
                $('.accordion a').on('click touchstart', this.accordionToggle);
            }
        }
    };

    App.CertificacaoProdutos = {
        submenuToggle: function(event) {
            event.preventDefault();

            var $handle = $(this),
                $sub    = $handle.next();

            $handle.toggleClass('active').promise().done(function(){
                $sub.slideToggle('slow');
            });
        },

        init: function() {
            if ($('.main').hasClass('certificacao-produtos')) {
                $('.nav-compulsoria a.has-sub').on('click touchstart', this.submenuToggle);
            }
        }
    };

    App.Treinamentos = {
        formSubmitLink: function() {
            var $formSubmit = $('.form-submit');

            if ($formSubmit.length) {
                $formSubmit.on('click touchstart', function(event) {
                    event.preventDefault();
                    $(this).closest('form').submit();
                });
            }
        },

        adicionaParticipante: function() {
            var $handle = $('.adiciona-participante');

            if ($handle.length) {
                $handle.on('click touchstart', function(event) {
                    event.preventDefault();
                    $('<div class="row" style="margin-top: 15px;"><label for="nomecompleto">nome completo</label><div class="formulario"><input type="text" name="nomecompleto[]" id="nomecompleto"></div></div><div class="row"><label for="email">e-mail</label><div class="formulario"><input type="email" name="email[]" id="email"></div></div>').insertBefore($handle.parent().parent());
                });
            }
        },

        toggleNovoEndereco: function() {
            var $handle = $('.form-novoendereco-handle');

            if ($handle.length) {
                $handle.on('change', function() {
                    var showForm = ($(this).val() === 'novo' ? true : false),
                        $form    = $(this).parent().parent().parent().next();

                    if (showForm) {
                        $form.slideDown();
                    } else {
                        $form.slideUp();
                    }
                })
            }
        },

        init: function() {
            this.formSubmitLink();
            this.adicionaParticipante();
            this.toggleNovoEndereco();
        }
    }

    App.init = function() {
        this.MobileButton.init();
        this.Home.init();
        this.Noticias.init();
        this.Contato.init();
        this.CertificacaoSistemas.init();
        this.CertificacaoProdutos.init();
        this.Treinamentos.init();
    };

    $(document).ready(function() {
        App.init();
    });

}(window, document, jQuery));
