    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'treinamentos-abertos';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-abertos.png" alt="">

                <h3>
                    TREINAMENTOS ABERTOS
                    <span class="area">área de atuação: <strong>QUALIDADE</strong></span>
                </h3>

                <div class="treinamento-descricao no-margin">
                    <div class="titulo">Nome do treinamento completo</div>
                    <div class="treinamento-data no-hover">
                        <div class="data">23 <strong>JUNHO</strong> 2015</div>
                        <div class="endereco">
                            <span><strong>São Paulo - SP</strong> | Auditório do Instituto Falcão Bauer</span>
                            <span class="icone">Rua do Endereço Completo, 123 · Bairro da Vila · Cidade, UF</span>
                        </div>
                    </div>
                </div>

                <h4>FORMULÁRIO DE INSCRIÇÃO</h4>
                <div class="inscricao-wrapper">
                    <div class="passos">
                        <div class="passo">
                            <span>CRIAR LOGIN</span>
                            <span class="numero">1</span>
                        </div>
                        <div class="passo active">
                            <span>INFORMAR<br>DADOS PESSOAIS</span>
                            <span class="numero">2</span>
                        </div>
                        <div class="passo">
                            <span>INFORMAR<br>DADOS DE COBRANÇA</span>
                            <span class="numero">3</span>
                        </div>
                        <div class="passo">
                            <span>CONFIRMAR E<br>FINALIZAR INSCRIÇÃO</span>
                            <span class="numero">4</span>
                        </div>
                    </div>

                    <h5>NOVO CADASTRO</h5>
                    <h6>DADOS PESSOAIS DO PARTICIPANTE</h6>
                    <form action="" method="post" class="form-inscricao-padrao">
                        <div class="row">
                            <label for="cpf">CPF</label>
                            <div class="formulario">
                                <input type="text" name="cpf" id="cpf">
                            </div>
                        </div>
                        <div class="row">
                            <label for="nomecompleto">nome completo</label>
                            <div class="formulario">
                                <input type="text" name="nomecompleto" id="nomecompleto">
                            </div>
                        </div>
                        <div class="row">
                            <label for="nomecracha">nome (crachá)</label>
                            <div class="formulario">
                                <input type="text" name="nomecracha" id="nomecracha">
                            </div>
                        </div>
                        <div class="row">
                            <label for="telefonefixo">telefone fixo</label>
                            <div class="formulario">
                                <input type="text" name="telefonefixo" id="telefonefixo">
                            </div>
                        </div>
                        <div class="row">
                            <label for="telefonecelular">telefone celular</label>
                            <div class="formulario">
                                <input type="text" name="telefonecelular" id="telefonecelular">
                            </div>
                        </div>
                        <div class="row">
                            <label for="cep">CEP</label>
                            <div class="formulario">
                                <input type="text" name="cep" id="cep">
                            </div>
                        </div>
                        <div class="row">
                            <label for="endereco">endereço</label>
                            <div class="formulario">
                                <input type="text" name="endereco" id="endereco">
                            </div>
                        </div>
                        <div class="row">
                            <label for="numero">número</label>
                            <div class="formulario">
                                <input type="text" name="numero" id="numero">
                            </div>
                        </div>
                        <div class="row">
                            <label for="complemento">complemento</label>
                            <div class="formulario">
                                <input type="text" name="complemento" id="complemento">
                            </div>
                        </div>
                        <div class="row">
                            <label for="bairro">bairro</label>
                            <div class="formulario">
                                <input type="text" name="bairro" id="bairro">
                            </div>
                        </div>
                        <div class="row">
                            <label for="cidade">cidade</label>
                            <div class="formulario">
                                <input type="text" name="cidade" id="cidade">
                            </div>
                        </div>
                        <div class="row">
                            <label for="uf">UF</label>
                            <div class="formulario">
                                <input type="text" name="uf" id="uf">
                            </div>
                        </div>
                        <div class="row">
                            <label for="empresa">empresa</label>
                            <div class="formulario">
                                <input type="text" name="empresa" id="empresa">
                            </div>
                        </div>
                        <div class="row">
                            <label for="cargo">cargo</label>
                            <div class="formulario">
                                <input type="text" name="cargo" id="cargo">
                            </div>
                        </div>
                        <div class="row">
                            <label for="atuacao">área de atuação</label>
                            <div class="formulario">
                                <input type="text" name="atuacao" id="atuacao">
                            </div>
                        </div>
                        <a href="#" class="form-submit">
                            <span>PROSSEGUIR: PASSO 3: INFORMAR DADOS DE COBRANÇA</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
