    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'treinamentos-abertos';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-abertos.png" alt="">

                <h3>
                    TREINAMENTOS ABERTOS
                    <span class="area">área de atuação: <strong>QUALIDADE</strong></span>
                </h3>

                <div class="treinamento-descricao">
                    <div class="titulo">Nome do treinamento completo</div>
                    <div class="row">
                        <label>OBJETIVO</label>
                        <div class="texto">
                            <ul>
                                <li>Interpretar e aplicar os requisitos da norma ISO 9001:200;</li>
                                <li>Qualificar os profissionais para atuarem como Auditores Internos do Sistema da Qualidade em suas organizações e também em fornecedores;</li>
                                <li>Desenvolver habilidades de trabalho em equipe;</li>
                                <li>Realizar auditorias internas e em fornecedores de acordo com a norma de referência.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <label>PÚBLICO-ALVO</label>
                        <div class="texto">
                            <p>Profissionais envolvidos no Sistema de Gestão da Qualidade que deverão atuar como auditores internos na empresa e em fornecedores.</p>
                        </div>
                    </div>
                    <div class="row">
                        <label>PRÉ-REQUISITO</label>
                        <div class="texto">
                            <p>N/A</p>
                        </div>
                    </div>
                    <div class="row">
                        <label>CERTIFICADO</label>
                        <div class="texto">
                            <p>Lorem ipsum dolor.</p>
                        </div>
                    </div>
                    <div class="row">
                        <label>CARGA HORÁRIA</label>
                        <div class="texto">
                            <p>24 horas</p>
                        </div>
                    </div>
                    <div class="row">
                        <label>CRONOGRAMA</label>
                        <div class="texto">
                            <p>6 aulas de 4 horas de duração</p>
                        </div>
                    </div>
                    <div class="row">
                        <label>HORÁRIOS</label>
                        <div class="texto">
                            <p>segunda a sexta das 18h às 22h</p>
                        </div>
                    </div>
                    <div class="row">
                        <label>INVESTIMENTO</label>
                        <div class="texto">
                            <p>R$2.000,00</p>
                        </div>
                    </div>
                    <div class="row">
                        <label>POLÍTICA DE DESCONTO</label>
                        <div class="texto">
                            <p>10% de desconto para pagamento até dia 20 de junho</p>
                        </div>
                    </div>
                    <div class="row">
                        <label>CONTEÚDO PROGRAMÁTICO</label>
                        <div class="texto">
                            <h5>Interpretação da norma ISO 9001:2008</h5>
                            <ul>
                                <li>Estrutura;</li>
                                <li>Conceitos;</li>
                                <li>Uso e aplicação;</li>
                                <li>Documentação.</li>
                            </ul>

                            <h5>Interpretação da norma ISO 9001:2008</h5>
                            <ul>
                                <li>Estrutura;</li>
                                <li>Conceitos;</li>
                                <li>Uso e aplicação;</li>
                                <li>Documentação.</li>
                            </ul>

                            <h5>Interpretação da norma ISO 9001:2008</h5>

                            <h5>Interpretação da norma ISO 9001:2008</h5>
                            <ul>
                                <li>Estrutura;</li>
                                <li>Conceitos;</li>
                                <li>Uso e aplicação;</li>
                                <li>Documentação.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="proximas-datas">
                    <h4>PRÓXIMAS DATAS</h4>
                    <div class="aviso">ESTE TREINAMENTO NÃO POSSUI DATA PREVISTA DE REALIZAÇÃO, REGISTRE SEU INTERESSE PARA RECEBER INFORMAÇÕES.</div>
                </div>
            </div>
        </div>
    </div>
