    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'in-company';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-incompany.png" alt="">

                <h3>TREINAMENTOS IN COMPANY</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, obcaecati voluptatum neque. Provident obcaecati assumenda qui nulla beatae quos ex!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi est eos ipsum nobis deserunt dolore ad dolor laborum voluptas exercitationem commodi numquam eveniet, accusamus et debitis, obcaecati eligendi animi dolores.</p>

                <h3>SOLICITE UMA PROPOSTA</h3>
                <form action="" id="form-proposta" method="post">
                    <select name="tema" id="tema">
                        <option value="" style="display:none;" disabled selected>tema [selecione]</option>
                        <option value="lorem">lorem</option>
                        <option value="ipsum">ipsum</option>
                    </select>
                    <input type="text" name="regiao" id="regiao" placeholder="região">
                    <input type="text" name="periodo" id="periodo" placeholder="período de realização">
                    <input type="text" name="participantes" id="participantes" placeholder="número de participantes">
                    <input type="text" name="empresa" id="empresa" placeholder="empresa">
                    <input type="text" name="solicitante" id="solicitante" placeholder="nome do solicitante">
                    <input type="text" name="cargo" id="cargo" placeholder="cargo">
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <input type="email" name="email" id="email" placeholder="e-mail">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem"></textarea>
                    <input type="submit" value="ENVIAR">
                </form>

                <a href="#" class="programacao-abertos">CONFIRA TAMBÉM NOSSA PROGRAMAÇÃO DE CURSOS ABERTOS »</a>
            </div>
        </div>
    </div>
