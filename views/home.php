    <div class="main home">
        <div class="banners">
            <div class="center">
                <div id="banners-cycle">
                    <a href="LINK-DO-SLIDE1" class="banners-slide" style="background-image: url('<?=$url?>assets/img/slide-placeholder.jpg');">
                        <div>
                            <p class="chamada">Conheça nossos serviços:</p>
                            <p class="titulo">Inovação na Construção Civil</p>
                            <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tincidunt semper nunc, vitae vehicula dui gravida vitae.</p>
                        </div>
                    </a>
                    <a href="LINK-DO-SLIDE2" class="banners-slide" style="background-image: url('<?=$url?>assets/img/slide-placeholder2.jpg');">
                        <div>
                            <p class="chamada">Conheça nossos serviços:</p>
                            <p class="titulo">Treinamentos e Cursos</p>
                            <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="treinamentos">
                <div class="titulo">
                    <p>Próximos Treinamentos:</p>
                    <a href="#">CONSULTAR AGENDA COMPLETA »</a>
                </div>

                <a href="#" class="treinamento-chamada">
                    <p class="data">
                        <span class="dia">02</span>
                        <span class="mes">junho</span>
                        <span class="ano">2015</span>
                    </p>
                    <p class="titulo">Formação de Auditor Interno ISO 9001:2008</p>
                </a>
                <a href="#" class="treinamento-chamada">
                    <p class="data">
                        <span class="dia">06</span>
                        <span class="mes">julho</span>
                        <span class="ano">2015</span>
                    </p>
                    <p class="titulo">Formação de Auditor Interno ISO 9001:2008</p>
                </a>
                <a href="#" class="treinamento-chamada">
                    <p class="data">
                        <span class="dia">22</span>
                        <span class="mes">agosto</span>
                        <span class="ano">2015</span>
                    </p>
                    <p class="titulo">Formação de Auditor Interno ISO 9001:2008</p>
                </a>
            </div>

            <div class="noticias">
                <a href="#" class="noticia">
                    <p class="titulo">Título da Notícia em Destaque</p>
                    <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed gravida turpis. Maecenas velit dui, commodo sit amet varius id, ultrices nec nisl...</p>
                    <span>LER MAIS »</span>
                </a>
                <a href="#" class="noticia">
                    <p class="titulo">Título da Notícia em Destaque</p>
                    <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed gravida turpis. Maecenas velit dui, commodo sit amet varius id, ultrices nec nisl...</p>
                    <span>LER MAIS »</span>
                </a>
                <a href="#" class="noticia">
                    <p class="titulo">Título da Notícia em Destaque</p>
                    <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed gravida turpis. Maecenas velit dui, commodo sit amet varius id, ultrices nec nisl...</p>
                    <span>LER MAIS »</span>
                </a>
            </div>

            <div class="validade-certificados">
                <a href="/validade">CONSULTAR VALIDADE DE CERTIFICADOS</a>
            </div>
        </div>
    </div>
