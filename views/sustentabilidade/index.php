    <div class="main sustentabilidade">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>SUSTENTABILIDADE</h2>

                <h3>SELOS</h3>
                <nav class="nav-selos">
                    <a href="<?=$url?>sustentabilidade" class="active">SELO ECOLÓGICO FALCÃO BAUER</a>
                    <a href="#">FALCÃO BAUER ECOLABEL BRASIL</a>
                    <a href="#">SUSTENTABILIDADE CONDOMINIAL</a>
                    <a href="#">CERTIFICAÇÃO DE ACESSIBILIDADE</a>
                    <a href="#">AVALIAÇÃO DO CICLO DE VIDA</a>
                </nav>

                <h3>OUTROS SERVIÇOS</h3>
                <nav class="nav-servicos">
                    <a href="<?=$url?>sustentabilidade/verificacao-de-teor">VERIFICAÇÃO DE TEOR DE COMPOSTOS ORGÂNICOS VOLÁTEIS</a>
                    <a href="#">VERIFICAÇÃO DE CONTEÚDO RECICLADO</a>
                    <a href="#">VERIFICAÇÃO DE MATERIAL REGIONAL</a>
                </nav>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-sustentabilidade.png" alt="">

                <h3>SELO ECOLÓGICO FALCÃO BAUER</h3>
                <img src="<?=$url?>assets/img/layout/selo-ecologico.png" class="selo" alt="">
                <div class="texto texto-selo">
                    <p><strong>O que é o Selo Ecológico?</strong></p>
                    <ul>
                        <li>Certificação voluntária de produto, acreditada pelo INMETRO e destinada a demonstrar o diferencial ecológico do produto determinado pelo fabricante ou solicitante da certificação.</li>
                        <li>Não há uma norma técnica específica e nem portaria do INMETRO que defina o processo de certificação.</li>
                        <li>As regras de certificação são definidas através de um procedimento específico, este é elaborado com base em normas técnicas e conceitos de sustentabilidade de modo a garantir a confiabilidade do processo de certificação.</li>
                        <li>Apesar de ser uma certifiação de produto, o procedimento prevê que sejam verificados alguns requisitos relativos ao processo produtivo, pois não basta  o produto possuir um diferencial ecológico, o seu processo produtivo também deve atender os princípios da sustentatbilidade.</li>
                    </ul>

                    <p><strong>Benefícios da Certificação</strong></p>
                    <ul>
                        <li>Comprovar seu diferencial ecológico através de uma instituição de terceira parte, maior credibilidade associada à emissão de um certificado de conformidade.</li>
                        <li>Uso do Selo Ecológico nos rótulos dos produtos para diferenciá-los dos concorrentes dando destaque ao desempenho ambiental do produto para seus consumidores.</li>
                        <li>Para determinados segmentos, a certificação acreditada pelo INMETRO fomenta algumas vantagens em alguns setores governamentais, por exemplo, pontuaçõe sem licitações e facilidades para empréstimos do BNDES.</li>
                        <li>Redução dos impactos negativos ao meio ambiente e à sociedade.</li>
                        <li>Enviar uma mensagem clara e confiável ao mercado consumidor, sobre as características ambientais do produto certificado para facilitar a escolha e fomentar o consumo sustentável.</li>
                    </ul>
                </div>

                <div class="documentos">
                    <span class="subtitulo">Documentos para download:</span>
                    <a href="#">Nome do documento para download</a>
                    <a href="#">Nome do documento para download</a>
                </div>
            </div>

            <div class="selos">
                <h4>Todos os Selos:</h4>
                <div>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecologico.png')"></div>
                        <span>SELO ECOLÓGICO FALCÃO BAUER</span>
                    </a>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecolabel.png')"></div>
                        <span>FALCÃO BAUER ECOLABEL BRASIL</span>
                    </a>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecolabel.png')"></div>
                        <span>SUSTENTABILIDADE CONDOMINIAL</span>
                    </a>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecolabel.png')"></div>
                        <span>CERTIFICAÇÃO DE ACESSIBILIDADE</span>
                    </a>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecolabel.png')"></div>
                        <span>AVALIAÇÃO DO CICLO DE VIDA</span>
                    </a>
                </div>
            </div>

            <div class="servicos">
                <h4>Outros serviços:</h4>
                <div>
                    <a href="#">
                        <img src="<?=$url?>assets/img/layout/img-selos-compostosorganicos.png">
                        <span>VERIFICAÇÃO DE TEOR DE COMPOSTOS ORGÂNICOS VOLÁTEIS</span>
                    </a>
                    <a href="#">
                        <img src="<?=$url?>assets/img/layout/img-selos-reciclado.png">
                        <span>VERIFICAÇÃO DE CONTEÚDO RECICLADO</span>
                    </a>
                    <a href="#">
                        <img src="<?=$url?>assets/img/layout/img-selos-regional.png">
                        <span>VERIFICAÇÃO DE MATERIAL REGIONAL</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
