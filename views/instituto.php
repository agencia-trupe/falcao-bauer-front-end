    <div class="main instituto">
        <div class="title">
            <div class="center">
                <h2>Instituto Falcão Bauer</h2>
            </div>
        </div>

        <div class="center">
            <div class="imagem">
                <img src="<?=$url?>assets/img/layout/img-institutofalcaobauer.png" alt="">
            </div>
            <div class="texto">
                <p>O Instituto Falcão Bauer da Qualidade (IFBQ), é um organismo brasileiro sem fins lucrativos, que atua na área de certificação de Produtos e Sistemas de Gestão.</p>
                <p>Em nossa expansão constituímos base e parcerias comerciais com organismos internacionais e somos acreditados pelo Inmetro para diversas certificações de produtos e sistemas.</p>
                <p>Somos uma das primeiras certificadoras de produtos no Brasil. Nossos clientes estão em todo território nacional, assim como no exterior, em países da Ásia, Europa, Américas do Norte, Central e Sul.</p>
                <p>A marca IFBQ está presente no dia a dia dos consumidores brasileiros, garantindo a qualidade dos produtos e serviços, principalmente no que se refere a segurança, a saúde e a preservação do meio ambiente.</p>
            </div>

            <div class="servicos">
                <h3>NOSSOS SERVIÇOS</h3>
                <a href="/treinamentos">
                    <img src="<?=$url?>assets/img/layout/img1-servicos-treinamentoscursos.png" alt="">
                    <p class="titulo">Treinamentos e Cursos</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia quia nobis, saepe laborum vel deleniti, recusandae sapiente corrupti quisquam omnis, dignissimos, dolore eum. Quasi at nihil delectus repellat tenetur. Dolor est sit libero iusto quibusdam eveniet odio nobis nam nulla voluptatem laboriosam, quidem aliquam suscipit provident vero atque voluptates. Sit.</p>
                </a>
                <a href="/certificacao-produtos">
                    <img src="<?=$url?>assets/img/layout/img2-servicos-certificacaoprodutos.png" alt="">
                    <p class="titulo">Certificação de Produtos</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, deleniti molestias alias eius, ab rerum laboriosam nisi! Optio cum incidunt qui, illo, itaque, possimus consequatur minus delectus, dolorem eum magnam ullam obcaecati unde earum harum. Unde explicabo nihil blanditiis est, molestiae, vero mollitia repellat delectus asperiores officiis autem molestias adipisci.</p>
                </a>
                <a href="/sustentabilidade">
                    <img src="<?=$url?>assets/img/layout/img3-servicos-sustentabilidade.png" alt="">
                    <p class="titulo">Sustentabilidade- Selos e serviços</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur perferendis itaque, ipsa sunt sit in cumque ratione odio? Non voluptate esse corrupti enim incidunt doloremque fugit, quod iusto quasi laborum molestias aliquid soluta aut, accusantium aperiam. Cum beatae aperiam, dolor recusandae officia at dolore facilis animi, harum, molestiae expedita consequuntur.</p>
                </a>
                <a href="/certificacao-sistemas">
                    <img src="<?=$url?>assets/img/layout/img4-servicos-sistemasgestao.png" alt="">
                    <p class="titulo">Certificação de Sistemas de Gestão</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, neque ea ab porro labore obcaecati dolores maxime tempore molestiae temporibus totam! Recusandae quam ea magni aperiam! Eos alias architecto veniam praesentium enim laborum nesciunt atque, repudiandae numquam labore dicta suscipit voluptate aspernatur. Ipsam itaque voluptate quasi ex consequuntur perspiciatis perferendis?</p>
                </a>
                <a href="/inovacao">
                    <img src="<?=$url?>assets/img/layout/img5-servicos-inovacaoconstrucaocivil.png" alt="">
                    <p class="titulo">Inovação na Construção Civil</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id fugit, a nulla voluptatem placeat? Iure asperiores suscipit quae magnam debitis, consequatur nisi repellendus, corporis laborum earum, porro. Eum quo recusandae asperiores perferendis quae, totam consectetur labore aliquid commodi impedit. Sapiente culpa tempora quidem delectus et minus porro quam quas! Sapiente.</p>
                </a>
            </div>
        </div>
    </div>
