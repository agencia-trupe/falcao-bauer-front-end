
    <footer>
        <div class="info">
            <div class="center">
                <div class="contato">
                    <div class="logo-footer"></div>
                    <p class="endereco">
                        Rua Aquinos, 111 · 3º Andar<br>
                        Água Branca · São Paulo/SP<br>
                        05036-070
                    </p>
                    <p class="telefone">(11) 3611·1729</p>
                </div>

                <div class="col">
                    <a href="#" class="menu">» INSTITUTO FALCÃO BAUER</a>
                    <a href="#" class="menu">» GRUPO FALCÃO BAUER</a>
                    <a href="#" class="menu">» NOTÍCIAS</a>
                    <a href="#" class="menu">» CONTATO</a>
                    <a href="#" class="menu">» TREINAMENTOS</a>
                    <a href="#" class="submenu">Abertos</a>
                    <a href="#">· Qualidade</a>
                    <a href="#">· Saúde e Segurança</a>
                    <a href="#">· Engenharia</a>
                    <a href="#">· Construção</a>
                    <a href="#">· Produtos</a>
                    <a href="#" class="submenu">In Company</a>
                </div>

                <div class="col">
                    <a href="#" class="menu">» CERTIFICAÇÃO DE PRODUTOS</a>
                    <a href="#" class="submenu">Certificações Compulsórias</a>
                    <a href="#">· Alimentos</a>
                    <a href="#">· Artigos Escolares</a>
                    <a href="#">· Artigos Infantis</a>
                    <a href="#">· Automotivo</a>
                    <a href="#">· Brinquedos</a>
                    <a href="#">· Combustível</a>
                    <a href="#">· Construção Civil</a>
                    <a href="#">· Eletrodomésticos</a>
                    <a href="#">· Eletromédicos</a>
                    <a href="#">· Implantes</a>
                    <a href="#">· Metal Mecânica</a>
                    <a href="#">· Pneus</a>
                    <a href="#">· Preservativos</a>
                    <a href="#" class="submenu">Certificações Voluntárias</a>
                </div>

                <div class="col">
                    <a href="#" class="menu">» SUSTENTABILIDADE</a>
                    <a href="#">· Ecolabel</a>
                    <a href="#">· Selo Ecológico</a>
                    <a href="#">· Sustentabilidade Condominal</a>
                    <a href="#">· Acessibilidade</a>
                    <a href="#">· Avaliação do Ciclo de Vida</a>
                    <a href="#">· Outros Serviços</a>
                    <a href="#" class="menu">» CERTIFICAÇÃO DE SISTEMAS DE GESTÃO</a>
                    <a href="#" class="menu">» INOVAÇÃO NA CONSTRUÇÃO CIVIL</a>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="center">
                <p>
                    © 2015 INSTITUTO FALCÃO BAUER · Todos os direitos reservados. |
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa.</a>
                </p>
            </div>
        </div>
    </footer>
