    <div class="main certificacao-produtos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>CERTIFICAÇÃO DE PRODUTOS</h2>

                <h3>CERTIFICAÇÃO COMPULSÓRIA</h3>
                <nav class="nav-compulsoria">
                    <a href="#">PNEUS</a>
                    <a href="#" class="has-sub open">AUTOMOTIVO</a>
                    <div class="submenu">
                        <a href="<?=$url?>certificacao-produtos/componentes-automotivos" class="active">Componentes Automotivos</a>
                        <a href="#">Vidro Laminado e Temperado Automotivo</a>
                        <a href="#">Rodas Automotivas</a>
                        <a href="#">Componentes de Bicicleta</a>
                    </div>
                    <a href="#">COMBUSTÍVEL</a>
                    <a href="#" class="has-sub">CAPACETE</a>
                    <div class="submenu">
                        <a href="#">Lorem ipsum dolor</a>
                        <a href="#">Lorem ipsum dolor</a>
                    </div>
                    <a href="#">SEGURANÇA PESSOAL</a>
                </nav>

                <h3>CERTIFICAÇÃO VOLUNTÁRIA</h3>
                <nav class="nav-voluntaria">
                    <a href="#">PRODUTOS DA CONSTRUÇÃO CIVIL</a>
                    <a href="#">VIDROS</a>
                    <a href="#">ROHS</a>
                    <a href="#">INFORMÁTICA</a>
                    <a href="#">ÁGUA MINERAL</a>
                </nav>
            </div>

            <div class="conteudo">
                <form action="" id="form-portaria">
                    <label for="portaria">Buscar pelo número da Portaria:</label>
                    <input type="text" name="portaria" id="portaria">
                    <input type="submit" value="BUSCAR">
                </form>
                <h3>
                    <span>CERTIFICAÇÃO COMPULSÓRIA · AUTOMOTIVO</span>
                    COMPONENTES AUTOMOTIVOS
                </h3>
                <div class="texto">
                    <p>FICHA TÉCNICA</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur reprehenderit, fugit? Perferendis illum dolor, reprehenderit dolorem dolore vero iste in accusamus dicta mollitia, expedita sit magni possimus quam ratione voluptatibus ad voluptatum explicabo error sed commodi! Beatae sint debitis obcaecati magni nemo laudantium saepe! Perferendis vitae tenetur adipisci rem alias, aperiam quia distinctio, corrupti sapiente reprehenderit quam mollitia veritatis. Veritatis voluptatem, doloribus officiis deserunt commodi nostrum repudiandae quas asperiores magnam, esse, voluptatum iste quibusdam pariatur minima facilis fugit incidunt laudantium. Beatae distinctio, dolorem corporis, quos quasi saepe aut iusto optio eligendi rerum sed molestias nobis dolore vitae tempore cupiditate labore.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit recusandae dolorum, quis fuga quia! Soluta odit doloribus sint incidunt animi alias a dignissimos dicta autem repellat delectus doloremque quia neque facilis molestiae sapiente, atque quis, ipsum molestias dolor vitae. Asperiores libero nulla consequuntur voluptatem, voluptate pariatur, qui modi est odit officiis, accusamus ea iure. Odio eaque consequatur officia, voluptas similique, illo, molestiae quae nemo voluptatum facere distinctio eius deserunt quis?</p>
                </div>
                <div class="documentos">
                    <span class="subtitulo">Documentos para download:</span>
                    <a href="#">Nome do documento para download</a>
                    <a href="#">Nome do documento para download</a>
                </div>

                <h4>SOLICITE SUA CERTIFICAÇÃO</h4>
                <form action="" id="form-certificacao">
                    <div class="row">
                        <label for="cnpj">CNPJ</label>
                        <div class="formulario">
                            <input type="text" name="cnpj" id="cnpj">
                        </div>
                    </div>
                    <div class="row">
                        <label for="razaosocial">razão social</label>
                        <div class="formulario">
                            <input type="text" name="razaosocial" id="razaosocial">
                        </div>
                    </div>
                    <div class="row">
                        <label for="contato">contato</label>
                        <div class="formulario">
                            <input type="text" name="contato" id="contato">
                        </div>
                    </div>
                    <div class="row">
                        <label for="telefone">telefone</label>
                        <div class="formulario">
                            <input type="text" name="telefone" id="telefone">
                        </div>
                    </div>
                    <div class="row">
                        <label for="email">e-mail</label>
                        <div class="formulario">
                            <input type="email" name="email" id="email">
                        </div>
                    </div>
                    <div class="row">
                        <label for="site">site</label>
                        <div class="formulario">
                            <input type="text" name="site" id="site">
                        </div>
                    </div>
                    <div class="row">
                        <label for="comentarios">comentários</label>
                        <div class="formulario">
                            <textarea name="comentarios" id="comentarios"></textarea>
                        </div>
                    </div>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>

        </div>
    </div>
