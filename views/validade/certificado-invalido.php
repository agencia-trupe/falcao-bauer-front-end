    <div class="main validade">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>CONSULTA · VALIDADE DE CERTIFICADOS</h2>
                <img src="<?=$url?>assets/img/layout/certificado.jpg" alt="">
            </div>

            <div class="conteudo">
                <h3>RESULTADO DA CONSULTA</h3>

                <div class="resultado">
                    <div class="row">
                        <label>NORMA:</label>
                        <p>NBR ISO 9001:2008</p>
                    </div>
                    <div class="row">
                        <label>EMPRESA:</label>
                        <p>L. A. Falcão Bauer - Cent. Tecnol. de Contr. de Qualidade Ltda.</p>
                    </div>
                    <div class="row">
                        <label>CERTIFICADO:</label>
                        <p>SQ-13481-12</p>
                    </div>
                    <div class="row situacao">
                        <p class="erro">Nenhum Certificado foi encontrado com os dados fornecidos.</p>
                        <p class="titulo">Situação do certificado:</p>
                        <p class="situacao">CERTIFICADO INEXISTENTE ou DADOS INCORRETOS</p>
                        <p>
                            Consulta realizada em: 20/06/2015
                        </p>
                    </div>
                </div>

                <p>Para mais informações ligue: 11 6789-5432</p>

                <a href="<?=$url?>validade">CONSULTAR OUTRO CERTIFICADO</a>
            </div>
        </div>
    </div>
