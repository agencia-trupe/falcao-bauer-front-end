    <div class="main grupo">
        <div class="title">
            <div class="center">
                <h2>Grupo Falcão Bauer</h2>
            </div>
        </div>

        <div class="center">
            <div class="texto">
                <p>Com mais de 59 anos de atividades, as empresas do Grupo Falcão Bauer prestam serviços de calibração de equipamentos, controle de qualidade e ensaios para uma grande variedade de materiais e produtos da indústria em geral, da construção civil, automotiva, de bens de consumo, brinquedos, dentre outros segmentos. Prestamos também serviços de consultorias nas áreas de engenharia civil, recuperação de estruturas, qualidade, meio ambiente e para a indústria petroquímica e de petróleo.</p>
                <ul>
                    <li>» CONSULTORIA - BNA</li>
                    <li>» ENGENHARIA E CONSTRUÇÃO</li>
                    <li>» ENSAIOS MECÂNICO E METALOGRÁFICO</li>
                    <li>» LABORATÓRIO QUÍMICO</li>
                    <li>» MEIO AMBIENTE</li>
                    <li>» PETRÓLEO E PETROQUÍMICO</li>
                </ul>

                <a href="http://www.falcaobauer.com.br/" target="_blank">
                    VISITE O SITE DO GRUPO FALCÃO BAUER PARA SABER MAIS SOBRE ESTES E OUTROS SERVIÇOS
                    <span>SITE GRUPO FALCÃO BAUER »</span>
                </a>
            </div>

            <div class="imagem">
                <img src="<?=$url?>assets/img/layout/img-grupofalcaobauer.png" alt="">
            </div>
        </div>
    </div>
