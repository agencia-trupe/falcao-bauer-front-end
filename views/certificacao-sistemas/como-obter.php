    <div class="main certificacao-sistemas">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>CERTIFICAÇÃO DE SISTEMAS DE GESTÃO</h2>
                <p>A certificação voluntária é uma decisão exclusiva das empresas fabricantes ou importadoras que desejam ter um diferencial de qualidade e demonstrar credibilidade de seus produtos e serviços. Os produtos certificados voluntariamente são comercializados com o Selo IFBQ, e caso haja um RAC - Regulamento de Avaliação da Conformidade - podem ostentar também o selo do INMETRO.</p>
                <a href="<?=$url?>certificacao-sistemas/como-obter" class="active">Confira os passos para obter uma certificação</a>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-certificacaosistemas.jpg" alt="">

                <div class="como-obter">
                    <h3>COMO OBTER A CERTIFICAÇÃO:</h3>
                    <p>O processo para a obtenção da certificação consiste em 3 passos:</p>

                    <div class="row">
                        <div class="passo passo-1"></div>
                        <p>A organização entra em contato com o IFBQ para solicitar a certificação. A partir das informações contidas na Solicitação de Certificação, o IFBQ analisa a pertinência da mesma e realiza um dimensionamento da auditoria para futura elaboração da memória de cálculo e emissão da proposta técnico-comercial.</p>
                        <p>Durante todo o processo de certificação, a organização conta com uma equipe técnica especializada do IFBQ para o esclarecimento de qualquer dúvida pertinente a ele.</p>
                    </div>

                    <div class="row">
                        <div class="passo passo-2"></div>
                        <p>O processo de certificação tem início após análise da documentação cadastral da organização. Sendo aprovada, o IFBQ define a equipe auditora e um plano de auditoria contendo a data, o programa da auditoria, o escopo requerido pelo solicitante e qualquer observação que seja relevante, para seu "de acordo".</p>
                        <p>Recebendo resposta positiva, a auditoria* é planejada e realizada. O relatório da mesma é emitido e, caso sejam detectadas não-conformidades, o solicitante deve tomar todas as ações corretivas necessárias, encaminhando-as ao IFBQ.</p>
                    </div>

                    <div class="row">
                        <div class="passo passo-3"></div>
                        <p>De posse do relatório de auditoria e das ações corretivas tomadas pela organização, o IFBQ realiza o processo decisório. Caso estes documentos sejam aprovados na análise, o certificado é emitido.</p>
                    </div>
                </div>

                <p>Durante a vigência do contrato, são realizados ensaios e auditorias periódicas (no mínimo uma por ano), no intuito de manutenção da certificação.</p>
                <p>A cada período de três anos, o IFBQ realiza uma auditoria de recertificação.</p>
                <p><em>*No caso de Sistemas, a auditoria é realizada em 2 fases.</em></p>
            </div>
        </div>
    </div>
